var popupStack = new Array();
var previousPopup;
var clickActivated = 0;



$(document).ready(
  function(){
    popupFilterActivatePopups();
  }
);



function popupFilterActivatePopups(){
  $('div.popup-hover-activate a.popup-title').unbind('mouseover').mouseover(function(event){ popupFilterMouseHover(this);});
  $('div.popup-click-activate a.popup-title').unbind('click').click(function(event){ popupFilterMouseClick(this);});
  $('div.popup-click-activate').children('a.popup-title').removeAttr('href');
}



function popupFilterShowPopup(popup){

  var anchor = popup.children('div.popup-anchor');
  var positioner = anchor.children('div.popup-positioner');
  var body = positioner.children('div.popup-body');

  var offsetX = 0;
  var offsetY = 0; 

  if (popup.hasClass('popup-origin-top-right') || popup.hasClass('popup-origin-bottom-right')){
    var width = popup.width();
    if (!width){
      width = popupFilterCleanNumber(popup.parent().css('width'));
    }
    offsetX = width;
  }
  if (popup.hasClass('popup-origin-bottom-left') || popup.hasClass('popup-origin-bottom-right')){
    offsetY = popup.children('.popup-title').height();
  }
  
  if (popup.hasClass('popup-expand-top-left') || popup.hasClass('popup-expand-bottom-left')){
    var width = body.width();
    if (!width){
      width = popupFilterCleanNumber(body.parent().css('width'));
    }
    offsetX -= body.width();
  }

  if (popup.hasClass('popup-expand-top-left') || popup.hasClass('popup-expand-top-right')){
    offsetY -= body.height();
    positioner.css(
      {
        height: body.height()+'px'
      }
    );
    body.css(
      {
        position: 'absolute',
        bottom: '0'
      }
    );
  }

  positioner.css(
    {
      left: offsetX + 'px',
      top: offsetY + 'px'
    }
  );
  
  // Hide previous popup
  if (previousPopup!=null && previousPopup.get(0) != popup.get(0)){
    previousPopup.children('.popup-anchor').css('z-index', 0).children('popup-positioner').children('.popup-body').hide();
  }

  anchor.css('z-index', 1000 + popupStack.length * 1000 + clickActivated * 1000);

  switch(true){
    case popup.hasClass('popup-effect-slide'): body.slideDown("medium"); break;
    case popup.hasClass('popup-effect-fade') : body.fadeIn("medium")   ; break;
    case popup.hasClass('popup-effect-slide-fade'): 
      body.animate(
        {
          height: 'show',
          opacity: 'show'
        },
        'medium'
      );
      break;
    default: body.show();
  }
}

function popupFilterHidePopup(popup){

  var anchor = popup.children('div.popup-anchor');
  var positioner = anchor.children('div.popup-positioner');
  var body = positioner.children('div.popup-body');

  switch(true){
    case popup.hasClass('popup-effect-slide'): body.slideUp(500); break;
    case popup.hasClass('popup-effect-fade'): body.fadeOut(500); break;
    case popup.hasClass('popup-effect-slide-fade'): 
      body.animate({
        height: 'hide',
        opacity: 'hide'
      }, 'medium'); 
      break;
    default: body.hide(); 
  }
}



function popupFilterMouseClick(title){

  var parent = $(title).parent();
  var anchor = parent.children('div.popup-anchor');
  var positioner = anchor.children('div.popup-positioner');
  var body = positioner.children('div.popup-body');

  var currentDisplay = body.css('display');

  if (currentDisplay == 'block'){
    clickActivated --;
    popupFilterHidePopup(parent);
  }

  if (currentDisplay == 'none'){
    clickActivated++;
    popupFilterShowPopup(parent);
  }
}

function popupFilterMouseHover(title){

  var parent = $(title).parent();
  var anchor = parent.children('div.popup-anchor');
  var positioner = anchor.children('div.popup-positioner');
  var body = positioner.children('div.popup-body');
  var id = parent.attr('id');
  
  // Ignore if already shown
  for(a=popupStack.length - 1; a >= 0; a--){
    if (popupStack[a] == id) return;
    // If not related, hide last
    if (parent.parents('#' + popupStack[a]).length == 0){
      var last = popupStack.pop();
      popupFilterHidePopup($('#' + last));
    }
  }

  popupStack.push(id);
  // Show
  popupFilterShowPopup(parent);

  if (popupStack.length == 1) $(document).mousemove(function(event){popupFilterMouseMove(event)});
}

function popupFilterMouseMove(event){
  var source = $(event.target);
  var id = source.attr('id');

  // Ignore if already shown
  for(a=popupStack.length - 1; a >= 0; a--){
    if (popupStack[a] == id) return;
    // If not related, hide last
    if (source.parents('#' + popupStack[a]).length == 0){
      var last = popupStack.pop();
      popupFilterHidePopup($('#' + last));
    }
  }
}



function popupFilterCleanNumber(toClean){
  return Number(('0' + toClean).replace(/[^0-9+-]+/, ''));
}