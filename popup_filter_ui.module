<?php
// $Id:



/* ----- Hook implementation ---- */



/**
 * Implementation of hook_enable
 */
function popup_filter_ui_enable(){
  _popup_filter_ui_flush_all();
}



/**
 * Implementation of hook_menu
 */
function popup_filter_ui_menu(){

  return array(
   
    'admin/settings/popup-filter/settings' => array(
      'access arguments' => array('administer popup filter'),
      'title' => 'Settings',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -1,
    ),

    'admin/settings/popup-filter/formats/list' => array(
      'access arguments' => array('administer popup filter'),
      'description' => 'Manage popup filter formats available to Views and CCK',
      'page arguments' => array('popup_filter_ui_form_formatters'),
      'page callback' => 'drupal_get_form',
      'title' => 'Popup filter formats',
      'type' => MENU_LOCAL_TASK,
    ),

    'admin/settings/popup-filter/formats/add' => array(
      'access arguments' => array('administer popup filter'),
      'description' => 'Add a popup filter format',
      'page arguments' => array('popup_filter_ui_form_format'),
      'page callback' => 'drupal_get_form',
      'title' => 'Add popup filter format',
      'type' => MENU_LOCAL_TASK,
      'weight' => 1,
    ),

    'admin/settings/popup-filter/formats/edit' => array(
      'access arguments' => array('administer popup filter'),
      'page arguments' => array('popup_filter_ui_form_format'),
      'page callback' => 'drupal_get_form',
      'title' => 'Editing Popup filter format',
      'type' => MENU_CALLBACK,
    ),

    /* Confirmations */

    'admin/settings/popup-filter/formats/confirm-delete' => array(
      'page callback' => 'drupal_get_form',
      'page arguments' => array(
        'popup_filter_ui_confirm',
        'Confirm format deletion',
        'Are you sure you want to delete the "%1" format?',
        array(
          'Confirm' => 'admin/settings/popup-filter/formats/delete/%1',
          '@Cancel' => 'admin/settings/popup-filter/formats/list',
        ),
      ),
      'access arguments' => array('administer popup filter'),
      'type' => MENU_CALLBACK,
    ),
    'admin/settings/popup-filter/formats/delete' => array(
      'page callback' => 'popup_filter_ui_delete_format',
      'access arguments' => array('administer popup filter'),
      'type' => MENU_CALLBACK,
    ),
    
    'admin/settings/popup-filter/formats/confirm-reset' => array(
      'page callback' => 'drupal_get_form',
      'page arguments' => array(
        'popup_filter_ui_confirm',
        'Confirm formats reset',
        'Are you sure you want to reset Popup filter formats to their default state?<br />This cannot be undone!',
        array(
          'Confirm' => 'admin/settings/popup-filter/formats/reset',
          '@Cancel' => 'admin/settings/popup-filter/formats/list',
        ),
      ),
      'access arguments' => array('administer popup filter'),
      'type' => MENU_CALLBACK,
    ),
    'admin/settings/popup-filter/formats/reset' => array(
      'page callback' => 'popup_filter_ui_reset',
      'access arguments' => array('administer popup filter'),
      'type' => MENU_CALLBACK,
    ),
    
  );

}



/**
 * Implementation of hook_field_formatter_info
 */
function popup_filter_ui_field_formatter_info() {

  $formatter_info = array();
  $formatters = popup_filter_ui_formatters();

  foreach($formatters as $label => $formatter){

    $formatter_info[str_replace(' ', '_', $label)] = array(
      'label' => $label,
      'field types' => array('nodereference'),
    );

  }
  
  return $formatter_info;
}



/* ---- Themeing hooks ---- */



/**
 * Implementation of hook_theme
 */
function popup_filter_ui_theme() {

  $formatters = popup_filter_ui_formatters();
  $theme = array();

  foreach($formatters as $label => $formatter){

    $theme['popup_filter_ui_formatter_'.str_replace(' ', '_', $label)] = array(
      'function' => 'theme_popup_node_field',
      'arguments' => array('element' => NULL),
    );

  }

  return $theme;
}



function theme_popup_node_field($element = NULL){

  $formatters = popup_filter_ui_formatters();

  $attributes = $formatters[str_replace('_', ' ', $element['#formatter'])];
  $attributes['node'] = $element['#item']['nid'];

  return $element['#item']['nid'] ? _popup_node($attributes) : '';
}



/* ---- Filter formatters ---- */



function popup_filter_ui_formatters($new_formatters = FALSE){

  static $formatters = FALSE;
  if ($new_formatters){
    $formatters = $new_formatters;
    variable_set('popup-filter-formatters', $formatters);
  }

  if (!$formatters){
    $formatters = variable_get('popup-filter-formatters', popup_filter_ui_defaults());
  }

  return $formatters;
}



function popup_filter_ui_defaults(){
  return array(

    'Popup node' => array(
       'origin' => 'bottom-left',
       'expand' => 'bottom-right',
       'effect' => 'none',
       'activate' => 'hover',
       'teaser' => FALSE,
       'links' => FALSE,
      ),

    'Popup node teaser' => array(
       'origin' => 'bottom-left',
       'expand' => 'bottom-right',
       'effect' => 'none',
       'activate' => 'hover',
       'teaser' => TRUE,
       'links' => FALSE,
      ),
    
    'Popup node links' => array(
       'origin' => 'bottom-left',
       'expand' => 'bottom-right',
       'effect' => 'none',
       'activate' => 'hover',
       'teaser' => FALSE,
       'links' => TRUE,
      ),
    
    'Popup node teaser links' => array(
       'origin' => 'bottom-left',
       'expand' => 'bottom-right',
       'effect' => 'none',
       'activate' => 'hover',
       'teaser' => TRUE,
       'links' => TRUE,
      ),
    
  );
}



/* ---- Forms ---- */



function popup_filter_ui_form_formatters(){

  $rows = array();
  $header = array('', t('Origin at'), t('Expand to'), t('Effect'), t('Activate'), t('Teaser'), t('Links'), t('Operations'));
  $formatters = popup_filter_ui_formatters();
  $form = array();

  foreach ($formatters as $label => $formatter){
    $rows[] = array(
      $label,
      $formatter['origin'],
      $formatter['expand'],
      $formatter['effect'],
      $formatter['activate'],
      ($formatter['teaser'] ? t('Yes') : t('No')),
      ($formatter['links'] ? t('Yes') : t('No')),
      l('Edit', 'admin/settings/popup-filter/formats/edit/'.$label).' '.
      l('Delete', 'admin/settings/popup-filter/formats/confirm-delete/'.$label)
    );
  }

  $form[] = array(
    '#value' => theme('table', $header, $rows, array()),
  );

  $form['op'] = array(
    '#type' => 'submit',
    '#value' => t('Reset to defaults'),
  );

  return $form;
}



function popup_filter_ui_form_formatters_submit(&$form, &$form_state){
  $form['#redirect'] = array('admin/settings/popup-filter/formats/confirm-reset');
}



function popup_filter_ui_form_format($state, $editing = FALSE){

  $form = array();
  $formatters = popup_filter_ui_formatters();
  $position_options = array('top-left' => t('Top left'), 'top-right' => t('Top right'), 'bottom-right' => t('Bottom right'), 'bottom-left' => t('Bottom left'));

  $form['label'] = array(
    '#default_value' => $editing,
    '#description' => t('The label that will be displayed in the format dropdown of node reference fields. May only contain letters numbers and spaces'),
    '#title' => t('Label'),
    '#type' => 'textfield',
  );

  $form['origin'] = array(
    '#default_value' => $formatters[$editing]['origin'],
    '#description' => t('The corner of the popup title that the actual popup will originate in'),
    '#title' => t('Origin at'),
    '#type' => 'select',
    '#options' => $position_options,
  );

  $form['expand'] = array(
    '#default_value' => $formatters[$editing]['expand'],
    '#description' => t('The direction that the popup should expand in, starting from the origin'),
    '#title' => t('Expand toward'),
    '#type' => 'select',
    '#options' => $position_options,
  );

  $form['effect'] = array(
    '#default_value' => $formatters[$editing]['effect'],
    '#title' => t('Effect'),
    '#type' => 'select',
    '#options' => array('none' => t('None'), 'slide' => t('Slide'), 'fade' => t('Fade'), 'slide-fade' => t('Slide and Fade')),
  );

  $form['activate'] = array(
    '#default_value' => $formatters[$editing]['activate'],
    '#title' => t('Activate'),
    '#type' => 'select',
    '#options' => array('hover' => t('Hover'), 'click' => t('Click')),
  );

  $form['teaser'] = array(
    '#default_value' => $formatters[$editing]['teaser'],
    '#title' => t('Teaser'),
    '#type' => 'checkbox',
  );

  $form['links'] = array(
    '#default_value' => $formatters[$editing]['links'],
    '#title' => t('Links'),
    '#type' => 'checkbox',
  );

  $form['op'] = array(
    '#type' => 'submit',
    '#value' => $editing ? t('Update format') : t('Add format'),
  );

  $form[] = array(
    '#value' => l(t('Cancel'), 'admin/settings/popup-filter/formats/list')
  );

  return $form;
}


function popup_filter_ui_form_format_validate(&$form, &$form_state){

  $values = $form_state['values'];
  $formatters = popup_filter_ui_formatters();

  if (!preg_match('/[A-Za-z0-9\ ]/', $values['label'])){
    form_set_error('label', t('The label may only contain letters, numbers and spaces.'));
  }

  if ($values['op'] == t('Add format') && isset($formatters[$values['label']])){
    form_set_error('label', t('Please provide a unique label'));
  }
}


function popup_filter_ui_form_format_submit(&$form, &$form_state){

  $values = $form_state['values'];
  $formatters = popup_filter_ui_formatters();

  $formatters[$values['label']] = array(
   'origin' => $values['origin'],
   'expand' => $values['expand'],
   'effect' => $values['effect'],
   'activate' => $values['activate'],
   'teaser' => $values['teaser'],
   'links' => $values['links'],
  );

  popup_filter_ui_formatters($formatters);
  _popup_filter_ui_flush_all();

  drupal_set_message(t('The "%format" format has been saved.', array('%format' => $values['label'])));

  $form['#redirect'] = array('admin/settings/popup-filter/formats/list');
}



/* ---- Actions ---- */



function popup_filter_ui_delete_format($format){

  $settings = popup_filter_ui_formatters();

  unset($settings[$format]);

  popup_filter_ui_formatters($settings);
  _popup_filter_ui_flush_all();

  drupal_set_message(t('The "%format" filter format has been deleted.', array('%format' => $format)));
  drupal_goto('admin/settings/popup-filter/formats/list');
}



function popup_filter_ui_reset(){

  popup_filter_ui_formatters(popup_filter_ui_defaults());
  _popup_filter_ui_flush_all();

  drupal_set_message(t('The Popup filter formats have been reset.'));
  drupal_goto('admin/settings/popup-filter/formats/list');
}



/* ---- Confirmation ---- */



function popup_filter_ui_confirm($state, $title, $message, $buttons, $arg_count = 1){

  $args = func_get_args();
  $args = array_slice(func_get_args(), count($args) - $arg_count - 1);

  foreach($args as $index=>$arg){
    $title = str_replace('%'.($index + 1), $arg, $title);
    $message = str_replace('%'.($index + 1), $arg, $message);

    foreach($buttons as $label=>$path){
      $buttons[$label] = str_replace('%'.($index + 1), $arg, $path);
    }
  }

  $form = array(array('#value' => '<p class="message">'.$message.'</p>'));

  foreach($buttons as $label=>$path){
    if (preg_match('/\@.+/', $label)){
      $form[] = array(
       '#value' => l(substr($label, 1), $path),
      );
    } else {
      $form[] = array(
        '#type' => 'submit',
        '#value' => t($label),
      );
    }
  }

  $form['buttons'] = array(
    '#type' => 'value',
    '#value' => $buttons,
  );

  drupal_set_title($title);

  return $form;
}



function popup_filter_ui_confirm_submit(&$form, &$form_state){
  drupal_goto($form_state['values']['buttons'][$form_state['values']['op']]);
}



/* ---- Utility functions ---- */



function _popup_filter_ui_flush_all(){
  $core = array('cache', 'cache_block', 'cache_filter', 'cache_page');
  $cache_tables = array_merge(module_invoke_all('flush_caches'), $core);
  foreach ($cache_tables as $table) {
    cache_clear_all('*', $table, TRUE);
  }
}


