var popupStack = new Array();
var previousPopup;
var clickActivated = 0;



/* ---- Initialization ---- */



$(document).ready(
  function(){
    popupFilterActivatePopups();
  }
);



function popupFilterActivatePopups(){

  if (Drupal.settings.popup_filter.keyboardFriendly){

    $('div.popup-hover-activate a.popup-title')
      .unbind('mouseover').mouseover(function(event){ popupFilterMouseHover(this);})
      .unbind('focus').focus(function(event){ popupFilterMouseHover(this);})
      .unbind('blur').blur(function(event){ popupFilterHidePopup($(this).parent()); popupStack.pop(); previousPopup = null;});
    $('div.popup-click-activate a.popup-title')
      .unbind('click').click(function(event){ popupFilterMouseClick(this); return false;});

    $('.popup-menu-title').keydown( function(e){ return popupFilterKeypress($(this), e);}).unbind('blur');

  } else {

    $('div.popup-hover-activate a.popup-title').unbind('mouseover').mouseover(function(event){ popupFilterMouseHover(this);});
    $('div.popup-click-activate a.popup-title').unbind('click').click(function(event){ popupFilterMouseClick(this); return false;});
    $('div.popup-click-activate').children('a.popup-title').removeAttr('href');

  }

}



/* ---- Hiding and showing ---- */



function popupFilterShowPopup(popup){

  var anchor = popup.children('div.popup-anchor');
  var positioner = anchor.children('div.popup-positioner');
  var body = positioner.children('div.popup-body');

  var offsetX = 0;
  var offsetY = 0; 

  if (popup.hasClass('popup-origin-top-right') || popup.hasClass('popup-origin-bottom-right')){
    var width = popup.width();
    if (!width){
      width = popupFilterCleanNumber(popup.parent().css('width'));
    }
    offsetX = width;
  }
  if (popup.hasClass('popup-origin-bottom-left') || popup.hasClass('popup-origin-bottom-right')){
    offsetY = popup.children('.popup-title').height();
  }
  
  if (popup.hasClass('popup-expand-top-left') || popup.hasClass('popup-expand-bottom-left')){
    var width = body.width();
    if (!width){
      width = popupFilterCleanNumber(body.parent().css('width'));
    }
    offsetX -= body.width();
  }

  if (popup.hasClass('popup-expand-top-left') || popup.hasClass('popup-expand-top-right')){
    offsetY -= body.height();
    positioner.css(
      {
        height: body.height()+'px'
      }
    );
    body.css(
      {
        position: 'absolute',
        bottom: '0'
      }
    );
  }

  positioner.css(
    {
      left: offsetX + 'px',
      top: offsetY + 'px'
    }
  );
  
  // Hide previous popup
  if (previousPopup!=null && previousPopup.get(0) != popup.get(0)){
    previousPopup.children('.popup-anchor').css('z-index', 0).children('popup-positioner').children('.popup-body').hide();
  }

  anchor.css('z-index', 1000 + popupStack.length * 1000 + clickActivated * 1000);

  switch(true){
    case popup.hasClass('popup-effect-slide'): body.slideDown("medium"); break;
    case popup.hasClass('popup-effect-fade') : body.fadeIn("medium")   ; break;
    case popup.hasClass('popup-effect-slide-fade'): 
      body.animate(
        {
          height: 'show',
          opacity: 'show'
        },
        'medium'
      );
      break;
    default: body.show();
  }
}

function popupFilterHidePopup(popup){

  var anchor = popup.children('div.popup-anchor');
  var positioner = anchor.children('div.popup-positioner');
  var body = positioner.children('div.popup-body');

  switch(true){
    case popup.hasClass('popup-effect-slide'): body.slideUp(500); break;
    case popup.hasClass('popup-effect-fade'): body.fadeOut(500); break;
    case popup.hasClass('popup-effect-slide-fade'): 
      body.animate({
        height: 'hide',
        opacity: 'hide'
      }, 'medium'); 
      break;
    default: body.hide(); 
  }
}



/* ---- Mouse Behaviour ---- */



function popupFilterMouseClick(title){

  var parent = $(title).parent();
  var anchor = parent.children('div.popup-anchor');
  var positioner = anchor.children('div.popup-positioner');
  var body = positioner.children('div.popup-body');

  var currentDisplay = body.css('display');

  if (currentDisplay == 'block'){
    clickActivated --;
    popupFilterHidePopup(parent);
  }

  if (currentDisplay == 'none'){
    clickActivated++;
    popupFilterShowPopup(parent);
  }
}

function popupFilterMouseHover(title){

  var parent = $(title).parent();
  var anchor = parent.children('div.popup-anchor');
  var positioner = anchor.children('div.popup-positioner');
  var body = positioner.children('div.popup-body');
  var id = parent.attr('id');
  
  // Ignore if already shown
  for(a=popupStack.length - 1; a >= 0; a--){
    if (popupStack[a] == id) return;
    // If not related, hide last
    if (parent.parents('#' + popupStack[a]).length == 0){
      var last = popupStack.pop();
      popupFilterHidePopup($('#' + last));
    }
  }

  popupStack.push(id);
  // Show
  popupFilterShowPopup(parent);

  if (popupStack.length == 1) $(document).mousemove(function(event){popupFilterMouseMove(event)});
}

function popupFilterMouseMove(event){
  var source = $(event.target);
  var id = source.attr('id');

  // Ignore if already shown
  for(a=popupStack.length - 1; a >= 0; a--){
    if (popupStack[a] == id) return;
    // If not related, hide last
    if (source.parents('#' + popupStack[a]).length == 0){
      var last = popupStack.pop();
      popupFilterHidePopup($('#' + last));
    }
  }
}



/* ---- Keyboard behaviour ---- */



function popupFilterKeypress(thisObject, e){

//alert(e.which);

  switch (e.which){
    // Enter submenu
    case 38: popupFilterMenuPrev(thisObject); return false;
    case 39: popupFilterMenuIn(thisObject); return false;
    case 40: popupFilterMenuNext(thisObject); return false;
    case 37: popupFilterMenuOut(thisObject); return false;
    case 9:
		  if (thisObject.hasClass('popup-menu-top-title')){

		    var topId;
		    var parent = thisObject.parent().attr('id');

		    do{
		       topId = popupStack.pop();
	         popupFilterHidePopup($('#'+topId));
	      } while (topId != parent && popupStack.length);

		  } else if (thisObject.hasClass('popup-menu-title')) {
		    thisObject.parents('.popup-menu-top-element').children('a')[0].focus();
		    return false;
		  } else {
		    var event = new Object();
		    event.source = thisObject;
		    popupFilterMouseMove(event);
		  }
  }

}



function popupFilterMenuIn(thisObject){

  firstChild = $(thisObject.siblings('.popup-anchor').children().children().children().get(0));

  if (firstChild.hasClass('popup-title')){
    firstChild[0].focus();
  }

  if (firstChild.hasClass('popup-element')){
    firstChild.children('a')[0].focus();
  }

}



function popupFilterMenuNext(thisObject){

  next = thisObject.next();
  if (next.size() == 0){
    next = thisObject.parent().next();
  }

  if (next.hasClass('popup-title')){
    next[0].focus();
  }

  if (next.hasClass('popup-element')){
    next.children('a')[0].focus();
  }

}



function popupFilterMenuPrev(thisObject){

  var prev = (thisObject.parent().hasClass('popup-element'))
    ? thisObject.parent().prev()
    : prev = thisObject.prev();

  if (prev.hasClass('popup-title')){
    prev[0].focus();
  }

  if (prev.hasClass('popup-element')){
    prev.children('a')[0].focus();
  }
}



function popupFilterMenuOut(thisObject){

  if (thisObject.hasClass('popup-menu-top-title')){
    return;
  }

  $(thisObject.parents('.popup-anchor')[0]).siblings('a')[0].focus();

}



/* ---- Utility ---- */



function popupFilterCleanNumber(toClean){
  return Number(('0' + toClean).replace(/[^0-9+-]+/, ''));
}